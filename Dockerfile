FROM openjdk:8-jre-alpine

WORKDIR /app

COPY . .

RUN mvn clean package -DskipTests

EXPOSE 8080

USER 1001

CMD ["java","-jar","/app/target/SpringBootExample.jar"]
